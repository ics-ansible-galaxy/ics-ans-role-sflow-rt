# ics-ans-role-sflow-rt

Ansible role to install sflow-rt.

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## Role Variables

```yaml
# Default sflow-rt home directory
sflow_rt_home: "/opt/sflow-rt"

# Default container home directory
sflow_rt_dir: "/sflow-rt/"

# Default docker container memory is set to 1G
sflow_rt_mem: 1G

# Name of docker container 
container_name: sflow-rt
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-sflow-rt
```

## License

BSD 2-clause
